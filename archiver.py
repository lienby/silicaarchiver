import os





def findPos(string,find):
    try:
        pos = string.index(find)
        return pos
    except:
        return None

def extract():
    archive = raw_input("Archive name: ")

    if not archive.endswith(".silica"):
        archive += ".silica"

    file = open(archive, "rb")
    data = file.read()
    file.close()
    while True:
        pos = findPos(data,"-[[{{")
        if pos == None:
            break
        pos += 5
        pos2 = findPos(data[pos:],"}}]]-")
        fileData = data[pos:]
        fileData = fileData[:pos2]
        pos2 += 10
        name = data[pos2:]
        name = name[:findPos(name,"-[[{{")]
        ## CREATE DIRS (IF ANY)
        if name.__contains__("/"):
            try:
                os.makedirs(os.path.dirname(name))
            except:
                pass


        ## SAVE THE FILE
        print("EXTRACTED FILE: " + name)
        extractedFile = open(name , "wb")
        extractedFile.write(fileData)
        extractedFile.close()
        data = data[pos2:]
        data = data[len(name):]
    file.close()


def add():
    archive = raw_input("Archive name: ")
    while True:


        if not archive.endswith(".silica"):
            archive += ".silica"

        global archiveprefix
        if os.path.exists(archive):
            file = open(archive, "rb")
            archiveprefix = file.read()
            file.close()

        filepath = raw_input("PATH TO FILE: ")
        file = open(filepath, "rb")
        filedata = file.read()
        file.close()
        file = open(archive, "wb")


        filename = raw_input("SAVE IN ARCHIVE AS: ")

        try:
            file.write(archiveprefix + '-[[{{'+filedata+'}}]]-'+filename)
        except NameError:
            file.write('-[[{{' + filedata + '}}]]-' + filename)
        file.close()
        if raw_input("Add another file? ").capitalize() == "N":
            break

if raw_input("""
.SILICA ARCHIVAL PROGRAM
(THERE ARE NO EXCEPTS IN THIS SO IT *WILL* BREAK IF U TRY TO BREAK IT)

1: Create Archive/Add Files To Archive
2: Extract .SILICA File

CHOICE: """) == "1":
    add()
else:
    extract()
