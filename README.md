# SIlicaArchiver
Tool To Create / Extract Archives in .SILICA format ^_^

Linux x64: https://bitbucket.org/SilicaAndPina/silicaarchiver/downloads/SilicaArchiver-linux64.tar.gz  
Windows x86: https://bitbucket.org/SilicaAndPina/silicaarchiver/downloads/SilicaArchiver-win32.zip   